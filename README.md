### Universidade Tuiuti do Parana ####
### Ci�ncia da Computa��o ###
### Disciplina: Teoria dos Grafos ###

### Exercicios/Trabalhos ###

* Periodo : 01/2017 -06/2017
* Ambiente : Desktop

### Ferramentas usadas ###

* Linguagem: C++
* IDE: QtCreator
* Plataforma : Linux/Windows

### Assuntos  ###
* Topologia em Arvore
* Arvore Binaria
* Arvore de busca Binaria
* �rvore bin�ria de busca balanceada
* Arvore AVL
* Arvore B
* Grafos Hamiltonianos
* Grafos Eulerianos
* Algoritmo Kruskal
* Algoritmo Prim
* Algoritmo Djiktra
* Algoritmo Bellman Ford
* Algoritmo Floyd Warshall
* Tabela de hashing


### Autor  ###

* Alan Azevedo Bancks

### Contato ###

* E-mail: dsalan@hotmail.com
