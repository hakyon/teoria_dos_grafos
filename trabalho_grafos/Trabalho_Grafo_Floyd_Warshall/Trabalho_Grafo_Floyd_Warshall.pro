TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    floyd_warshall.cpp \
    main.cpp \
    leitura.cpp

HEADERS += \
    floyd_warshall.h \
    aresta.h \
    leitura.h
