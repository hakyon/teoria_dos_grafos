#ifndef FLOYD_WARSHALL_H
#define FLOYD_WARSHALL_H

#include <iostream>
#include <vector>
#include <iomanip>

#include "aresta.h"

using namespace std;

class FloydWarshall{
public:

    //Inicializa as variaveis internas
    FloydWarshall(vector<Aresta*> arestas,unsigned int INFINITO,int maiorVertice);
    ~FloydWarshall();

    void imprimeTotais();
    void imprimeCaminho(int origem, int destino);
    void imprimeCaminhos();

private:
    int _INFINITO;
    int **_matrizTotais;
    int _maiorVertice;
    int **_matrizCaminhos;
    vector<Aresta*> _arestas;

};


#endif // FLOYD_WARSHALL_H
