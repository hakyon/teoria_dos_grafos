#include "floyd_warshall.h"

FloydWarshall::FloydWarshall(vector<Aresta*> arestas,unsigned int INFINITO,int maiorVertice) {
    _arestas = arestas;
    _INFINITO = INFINITO;
    _maiorVertice = maiorVertice;
    _matrizTotais = new int*[_maiorVertice];
    _matrizCaminhos = new int*[_maiorVertice];

    for(int i=0; i<_maiorVertice; i++) {
        _matrizTotais[i] = new int[_maiorVertice];
        _matrizCaminhos[i] = new int[_maiorVertice];
        for(int j=0; j<_maiorVertice; j++) {
            if(i==j) { //Inicializa os valores na matriz
                _matrizTotais[i][j] = 0;
            } else {
                _matrizTotais[i][j] = _INFINITO;
                _matrizCaminhos[i][j] = _INFINITO;
            }

        }
    }

    for(unsigned int i=0; i<arestas.size(); i++) {
        _matrizTotais[_arestas[i]->getOrigem()-1][_arestas[i]->getDestino()-1] = _arestas[i]->getPeso();
        _matrizTotais[_arestas[i]->getDestino()-1][_arestas[i]->getOrigem()-1] = _arestas[i]->getPeso();
    }

    //Ajusta a matriz de predecessores
    for(int i=0;i<_maiorVertice;i++){
        for(int j=0;j<_maiorVertice;j++){
            if(_matrizTotais[i][j] <_INFINITO) {
                _matrizCaminhos[i][j] = i+1;//Coloca o caminho inicial
            }
        }
    }

    //Calcula menor caminho
    for(int k=0; k<_maiorVertice; k++) {
        for(int i=0; i<_maiorVertice; i++) {
            for(int j=0; j<_maiorVertice; j++) {
                if((_matrizTotais[i][j] > _matrizTotais[i][k] + _matrizTotais[k][j]) ) {
                    _matrizTotais[i][j] = _matrizTotais[i][k] + _matrizTotais[k][j];//Arruma o calculo total do peso
                    _matrizCaminhos[i][j] = _matrizCaminhos[k][j];//Arruma a matriz de predecessores (caminhos)
                }//Fim IF
            }//Fim j
        }//Fim i
    }//Fim k
    cout << endl;

}

FloydWarshall::~FloydWarshall() {

    for(int i=0; i<_maiorVertice; i++) { //Apaga as matrizes criadas dinamicamente
        delete [] _matrizTotais[i];
        delete [] _matrizCaminhos[i];
    }
    delete [] _matrizTotais;
    delete [] _matrizCaminhos;

}

void FloydWarshall::imprimeTotais() {
    //Imprime as colunas
    cout << "    ";
    for(int i=0; i<_maiorVertice; i++) {
        if(i/10 > 1) {
            cout << " [" << i+1 << "] ";
        } else {
            cout << " [" << i+1 << "]  ";
        }

    }
    cout << endl;
    for(int i=0; i<_maiorVertice; i++) {
        cout << "[" << i+1 << "] ";//Imprime o titulo da linha
        for(int j=0; j<_maiorVertice; j++) { //Imprime os destinos dessa origem
            if(i/100 > 1){
                cout << " " << _matrizTotais[i][j] << "  ";
            }else if(i/10 > 1) {
                cout << "  " << _matrizTotais[i][j] << "  ";
            } else if(_matrizTotais[i][j] == _INFINITO){
                cout << " INFINITO  ";
            }else{
                cout << "  " << _matrizTotais[i][j] << "   ";
            }
        }
        cout << endl;//Quebra de linha
    }
}
void FloydWarshall::imprimeCaminhos() {
    //Imprime as colunas
    cout << "    ";
    for(int i=0; i<_maiorVertice; i++) {
        if(i/10 > 1) {
            cout << " [" << i+1 << "] ";
        } else {
            cout << " [" << i+1 << "]  ";
        }

    }
    cout << endl;
    for(int i=0; i<_maiorVertice; i++) {
        cout << "[" << i+1 << "] ";//Imprime o titulo da linha
        for(int j=0; j<_maiorVertice; j++) { //Imprime os destinos dessa origem
            if(i/100 > 1){
                cout << " " << _matrizCaminhos[i][j] << "  ";
            }else if(i/10 > 1) {
                cout << "  " << _matrizCaminhos[i][j] << "  ";
            } else if(_matrizCaminhos[i][j] == _INFINITO){
                cout << " INFINITO  ";
            }else{
                cout << "  " << _matrizCaminhos[i][j] << "   ";
            }
        }
        cout << endl;//Quebra de linha
    }
}
void FloydWarshall::imprimeCaminho(int origem, int destino){
    int i,j,aux;
    if(origem < 0 || destino < 0 || origem > _maiorVertice || destino > _maiorVertice){
        cout << "A origem ou destino inválidos!" << endl;
        return;
    }
    if(_matrizTotais[origem][destino] == _INFINITO){
        cout << "Caminho não calculado ou impossivel de ser alcançado!" << endl;
        return;
    }
    //Ajusta para trabalhar com as matrizes
    i = origem -1;
    aux = j = destino - 1;
    cout << "Custo e caminho de " << origem << " para " << destino << endl;
    cout << "Custo total: " << _matrizTotais[i][j] << endl;

    while(aux != i ){//Percorre a matriz de predecessores pegando o caminho
        if(aux != j){
            cout << "<-";
        }
        cout << aux+1 << " " ;
        aux = _matrizCaminhos[i][aux]-1;
    }

    cout << "<-" << aux+1 << endl;

}
