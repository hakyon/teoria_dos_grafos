#ifndef LEITURA_H
#define LEITURA_H

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "aresta.h"

using namespace std;

class Leitura{
public:
    Leitura(string path){
        //Seta o path
        _path = path;
    };
    ~Leitura(){
        //Apaga o vetor de arestas
        for(int i=0;i<_aux.size();i++){
            delete _aux[i];
        }
    }

    bool readData();
    int getMaiorVertice(){
        //Retorna o maior indice de vertice localizado
        return _maior;
    };
    vector<Aresta*> getArestas(){
        //Retorna todas as arestas carregadas
        return _aux;
    };

    int getMaiorPeso(){
        return _maiorPeso;
    }

private:

    int _maior;
    int _maiorPeso;
    vector<Aresta*> _aux;
    string _path;
    ifstream file;
};

#endif // LEITURA_H
