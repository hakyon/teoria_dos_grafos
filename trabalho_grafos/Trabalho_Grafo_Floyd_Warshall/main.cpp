#include <iostream>
#include "leitura.h"
#include "floyd_warshall.h"


using namespace std;

int main()
{
    string arquivo;
    arquivo = "grafo.txt";
    Leitura dados(arquivo);
    vector<Aresta*> arestasCarregadas;

    try{
        dados.readData();
    }catch(const char *err){
        cout << err;
    }

    arestasCarregadas = dados.getArestas();

    FloydWarshall floyd(dados.getArestas(),
                        dados.getMaiorPeso()+1,
                        dados.getMaiorVertice());
    floyd.imprimeCaminho(1,4);
    return 0;
}
