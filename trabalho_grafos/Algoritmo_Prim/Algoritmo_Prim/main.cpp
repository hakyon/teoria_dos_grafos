#include <iostream>

#include "prim.h"

using namespace std;

#define ARQUIVO_LEITURA "grafo.txt"

int ConversaoASCII(int v);

int main()
{
    int V = 9;
    Prim g(V);

    string line;
    int i=0;
    ifstream input("grafo.txt");
    while (!input.eof()){
        if (input.fail()) {
            cout <<"Falhou a leitura";
        }
        input >> line;
        g.AdicionarVertice(ConversaoASCII(line[0]),ConversaoASCII(line[2]),ConversaoASCII(line[4]));
        i++;
    }


    g.prim();

    return 0;
}

int ConversaoASCII(int v) // nao deveria existir isso
{
    switch (v) {
    case 48:
        return 0;
        break;
    case 49:
        return 1;
        break;
    case 50:
        return 2;
        break;
    case 51:
        return 3;
        break;
    case 52:
        return 4;
        break;
    case 53:
        return 5;
        break;
    case 54:
        return 6;
        break;
    case 55:
        return 7;
    case 56:
        return 8;
        break;
    case 57:
        return 9;
        break;
    default:
        break;
    }
    return 0;
}
