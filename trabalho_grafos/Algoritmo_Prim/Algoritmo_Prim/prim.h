#ifndef PRIM_H
#define PRIM_H

#include<bits/stdc++.h>
using namespace std;
# define INFINITO 100000

typedef pair<int, int> iPair;  // dessa vez vamos usar isso

class Prim
{
    int V;  // numero de vertices

    list< pair<int, int> > *adjacente;

public:
    Prim(int V);

    // Funcao que adiciona vertice ao grafo
    void AdicionarVertice(int u, int v, int w);

    void prim(); // arvore de expansao minima
};


#endif // PRIM_H
