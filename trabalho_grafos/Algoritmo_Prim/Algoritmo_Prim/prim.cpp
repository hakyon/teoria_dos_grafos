#include "prim.h"

Prim::Prim(int V)
{
    this->V = V;
    adjacente = new list<iPair> [V];
}

void Prim::AdicionarVertice(int u, int v, int w)
{
    adjacente[u].push_back(make_pair(v, w));
    adjacente[v].push_back(make_pair(u, w));
}



void Prim::prim() // Arvore de expansao minima
{
    // Criando um fila de prioridades
    priority_queue< iPair, vector <iPair> , greater<iPair> > pq;

    int src = 0; // começa passando zero

    vector<int> key(V, INFINITO);

    //  armazenar a matriz pai,
    vector<int> parent(V, -1);

    // acompanhar os vertices
    vector<bool> inAEM(V, false);

    // Insire uma própria fonte na fila de prioridade e inicializa  com chave 0
    pq.push(make_pair(0, src));
    key[src] = 0;

    while (!pq.empty())
    {
        //  vertice adiciona um par na chave minima

        // in pair)
        int u = pq.top().second;
        pq.pop();

        inAEM[u] = true;  // inclui no vertice

        list< pair<int, int> >::iterator i;
        for (i = adjacente[u].begin(); i != adjacente[u].end(); ++i)
        {
            // Pega conteund do vertice e seu peso adjacenteacente
            int v = (*i).first;
            int peso = (*i).second;


            if (inAEM[v] == false && key[v] > peso)
            {
                //Atualiza
                key[v] = peso;
                pq.push(make_pair(key[v], v));
                parent[v] = u;
            }
        }
    }

    //
    for (int i = 1; i < V; ++i)
        printf("%d - > %d\n", parent[i], i); // imprime
}
