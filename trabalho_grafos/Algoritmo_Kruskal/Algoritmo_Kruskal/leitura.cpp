#include "leitura.h"
bool Leitura::readData(){


    //Variaveis para leitura dos dados
    int origem, destino, peso, maior=0, maiorPeso=0;
    char tmp[256]={'\0'};
    string ori,dst,pes;
    int qtdeLinhas;
    int linhaAtual=0;

    //Abre o arquivo e testa a abertura
    file.open(_path.c_str());
    if(!file){//Se ocorrer um erro sobe excessao
        throw "Falha ao carregar o arquivo!";
    }
    //Vai para o inicio do arquivo
    file.clear();
    file.seekg(0, ios::beg);

    //Pega a quantidade de linhas do arquivo
    for (qtdeLinhas = 0; file.getline(tmp, 256); qtdeLinhas++);

    //Vai para o inicio do arquivo
    file.clear();
    file.seekg(0, ios::beg);


    //Realiza a leitura do arquivo
    while(!file.eof()){
        ori = "";
        dst = "";
        pes = "";
        //Pega uma linha e joga num buffer temporario
        file.getline(tmp,256);

        //Se for fim de linha sai do laço
        if(file.eof())
            break;


        int i;
        for(i=0;i<256;i++){//Le a origem
            //Se acabou a linha e não achou a origem, então tem algo errado, aborta
            if(tmp[i] == '\n' || tmp[i] == '\0'){
                file.close();
                throw "Arquivo no formato invalido!";
            }

            //Se achou o fim do primerio valor vai para o proximo
            if(tmp[i] == ';'){
                i++;
                break;
            }
            ori += tmp[i];
        }

        for(;i<256;i++){//Le o destino
            //Se acabou a linha e não achou o destino, então tem algo errado, aborta
            if(tmp[i] == '\n' || tmp[i] == '\0'){
                file.close();
                throw "Arquivo no formato invalido!";
            }

            //Se achou o fim do primerio valor vai para o proximo
            if(tmp[i] == ';'){
                i++;
                break;
            }
            dst += tmp[i];
        }

        for(;i<256;i++){//Le o peso
            //Se acabou a linha ou achou ; então acabou o carregamento
            if(tmp[i] == '\n' || tmp[i] == '\0' || tmp[i] == ';'){
                break;
            }
            pes += tmp[i];
        }


        //Converte de texto para int
        origem = stoi(ori);
        destino = stoi(dst);
        peso = stoi(pes);


        //Verifica qual o maior valor de aresta para gerar a matriz
        if(origem > maior)
            maior = origem;
        if(destino > maior)
            maior = destino;
        if(peso > maiorPeso)
            maiorPeso = peso;
        _aux.push_back(new Aresta(origem,destino,peso));
    }

    if (maior == 0){
        return false;
    }

    _maior = maior;
    _maiorPeso = maiorPeso;
    return true;
}
