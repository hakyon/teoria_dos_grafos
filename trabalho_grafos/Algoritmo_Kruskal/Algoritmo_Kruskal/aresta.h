#ifndef _ARESTA_H_
#define _ARESTA_H_

#include <fstream>

using namespace std;

class Aresta{
public:

    Aresta(){}
    Aresta(int origem,int destino, int peso):
        _origem(origem),_destino(destino),_peso(peso){}


    //isolamento
    void setOrigem(int origem){
        _origem = origem;
    }
    int getOrigem(){return _origem;}
    void setDestino(int destino){_destino = destino;}
    int getDestino(){return _destino;}
    void setPeso(int peso){_peso = peso;}
    int getPeso(){return _peso;}

    //Sobrecarrega o operador << para facilitar exibição em tela ou gravação em arquivo
    friend ostream& operator << (ostream& out, Aresta &aresta){
        out << aresta._origem << ";" << aresta._destino << ";" << aresta._peso << endl;
        return out;
    }

    bool operator < (const Aresta& aresta)
    {
        return (_peso < aresta._peso);
    }

private:
    int _origem;
    int _destino;
    int _peso;
};

#endif // _ARESTA_H_
