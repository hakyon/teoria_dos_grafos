#include "kruskal.h"

//Kruskal::Kruskal()
//{
//    printf("\n Kruskal \n");
//    printf("\n Informa o numero de vertices:");
//    scanf("%d",&n);
//    printf("\nEntre com os valores da matrs adjacente:\n");
//    for(i=1;i<=n;i++)
//    {
//        for(j=1;j<=n;j++)
//        {
//            scanf("%d",&cost[i][j]);
//            if(cost[i][j]==0)
//                cost[i][j]=999;
//        }
//    }
//    printf("Os vertice da arvore de extensao de custo minimo sao:\n");
//    while(ne < n)
//    {
//        for(i=1,min=999;i<=n;i++)
//        {
//            for(j=1;j <= n;j++)
//            {
//                if(cost[i][j] < min)
//                {
//                    min=cost[i][j];
//                    a=u=i;
//                    b=v=j;
//                }
//            }
//        }
//        u=Encontrar(u);
//        v=Encontrar(v);
//        if(Unidade(u,v))
//        {
//            printf("%d Vertice [%d,%d] =%d\n",ne++,a,b,min);
//            mincost +=min;
//        }
//        cost[a][b]=cost[b][a]=999;
//    }
//    printf("\n\t Custo Minimo = %d\n",mincost);
//    getch();


//}

//int Kruskal::Encontrar(int i)
//{
//    while(parent[i])
//    i=parent[i];
//    return i;
//}
//int Kruskal::Unidade(int i,int j)
//{
//    if(i!=j)
//    {
//        parent[j]=i;
//        return 1;
//    }
//    return 0;
//}
// outro modelo

#include "kruskal.h"

Kruskal::Kruskal(vector<Aresta*> arestas,unsigned int INFINITO,int maiorVertice){
    _arestas = arestas;
    _maiorVertice = maiorVertice;
    _ciclo = new int[_maiorVertice];
    _pesoTotal = 0;

    //Faz que com cada vértice tenha ele mesmo como destino
    // [1]->[1]
    for(int i=0;i<_maiorVertice+1;i++){
        _ciclo[i] = i;
    }

    //Ordenaçao
    sort(_arestas.begin(),_arestas.end());
    for(int i=0;i<_arestas.size();i++){
        cout << *_arestas[i];
    }

    //Inicia Kruskal
    for(int i=0;i<_maiorVertice;i++){
        if(pai(_arestas[i]->getOrigem()) != pai(_arestas[i]->getDestino())){
            //Imprime pelo menor caminho encontrado, típico de um algoritmo guloso
            if(_arestas[i]->getOrigem() < _arestas[i]->getDestino()){
                cout << "Caminho(" << i <<") " << _arestas[i]->getOrigem() << " -> " << _arestas[i]->getDestino() << endl;
            }else{
                cout << "Caminho(" << i <<") " << _arestas[i]->getDestino() << " -> " <<  _arestas[i]->getOrigem() << endl;
            }
            _pesoTotal += _arestas[i]->getPeso();
            unir(_arestas[i]->getOrigem(), _arestas[i]->getDestino());
        }
    }
    cout << "Peso total do caminho: " << _pesoTotal << endl;
}

void Kruskal::unir(int origem, int destino){
    _ciclo[pai(origem)] = pai(destino);
}
int Kruskal::pai(int arestaAtual){
    if(_ciclo[arestaAtual] == arestaAtual){
        return arestaAtual;
    }
    _ciclo[arestaAtual] = pai(_ciclo[arestaAtual]);
    return _ciclo[arestaAtual];
}
