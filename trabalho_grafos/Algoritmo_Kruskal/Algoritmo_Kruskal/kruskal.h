#ifndef KRUSKAL_H
#define KRUSKAL_H

#include <iostream>
#include <vector>
#include <iomanip>
#include <algorithm>

#include "aresta.h"
#include "leitura.h"

using namespace std;
class Kruskal{
public:

    //Inicializa as variaveis internas
    Kruskal(vector<Aresta*> arestas,unsigned int INFINITO,int maiorVertice);
    void unir(int origem, int destino);

private:
    vector<Aresta*> _arestas;
    int pai(int arestaAtual);
    int _maiorVertice;
    int _pesoTotal;
    int *_ciclo;
};

#endif // KRUSKAL_H
