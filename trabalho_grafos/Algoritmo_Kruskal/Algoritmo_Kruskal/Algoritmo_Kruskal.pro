TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    kruskal.cpp \
    leitura.cpp

HEADERS += \
    kruskal.h \
    aresta.h \
    leitura.h
