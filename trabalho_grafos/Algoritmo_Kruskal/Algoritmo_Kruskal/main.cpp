#include <iostream>
#include "kruskal.h"
#include "leitura.h"
#include <iostream>

using namespace std;

#define ARQUIVO_DE_LEITURA "grafo.txt"

int main()
{
    string arquivo;

    arquivo = ARQUIVO_DE_LEITURA;
    Leitura dados(arquivo);
    vector<Aresta*> arestasCarregadas;

    try
    {
        dados.readData();
    }
    catch(const char *erro)
    {
        cout << erro;
    }
    arestasCarregadas = dados.getArestas();
    //cout << "Carregou"<<endl;
    Kruskal kruskal(dados.getArestas(),
                    dados.getMaiorPeso()+1,
                    dados.getMaiorVertice());

    return 0;
}
