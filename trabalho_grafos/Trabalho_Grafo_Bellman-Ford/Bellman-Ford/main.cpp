#include <iostream>
#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>

#include "bellman_ford.h"

using namespace std;

struct Aresta
{
    int origem, destino, peso;
};

struct Grafo
{
    int V, E;  // V-> numero de verticess, E-> Numero de arestas
    struct Aresta* aresta; // representacao do grafo em uma estrutura
};
typedef Grafo Grafo;

//afo CriandoGrafo(int V, int E);
//Grafo *grafo = bellman->CriandoGrafo(V, E);

void Executando(Grafo *grafo, int origem);
void Imprimir(int distancia[],int n);
struct Grafo* CriandoGrafo(int V, int E);

int main()
{

    Bellman_Ford *bellman;
    int V = 5; // Numero de vertices
    int E = 8; // Numero de arestas
    struct Grafo* grafo =  CriandoGrafo(V, E);
    // Grafo *grafo = bellman->CriandoGrafo(V, E);
    string line;
    int i=0;
    ifstream input("grafo.txt");
    while (!input.eof()){
        if (input.fail()) {
            cout <<"Falhou a leitura";
        }
        input >> line;
        grafo->aresta[i].origem = bellman->ConversaoASCII(line[0]);
        grafo->aresta[i].destino = bellman->ConversaoASCII(line[2]);
        grafo->aresta[i].peso = bellman->ConversaoASCII(line[4]);
        i++;
    }

    Executando(grafo, 0);

    return 0;
}
struct Grafo* CriandoGrafo(int V, int E)
{
    struct Grafo* grafo =(struct Grafo*) malloc( sizeof(struct Grafo) );
    grafo->V = V;
    grafo->E = E;

    grafo->aresta = (struct Aresta*) malloc( grafo->E * sizeof( struct Aresta ) );

    return grafo;
}

void Executando(struct Grafo *grafo, int origem)
{
    int V = grafo->V;
    int E = grafo->E;
    int distancia[V];

    for (int i = 0; i < V; i++)
        distancia[i] = INT_MAX;
    distancia[origem] = 0;


    for (int i = 1; i <= V - 1; i++)
    {
        for (int j = 0; j < E; j++)
        {
            int u = grafo->aresta[j].origem;
            int v = grafo->aresta[j].destino;
            int peso = grafo->aresta[j].peso;
            if (distancia[u] != INT_MAX && distancia[u] + peso < distancia[v])
                distancia[v] = distancia[u] + peso;
        }
    }

    for (int i = 0; i < E; i++)
    {
        int u = grafo->aresta[i].origem;
        int v = grafo->aresta[i].destino;
        int peso = grafo->aresta[i].peso;
        if (distancia[u] != INT_MAX && distancia[u] + peso < distancia[v])
            printf("Grafo contem peso negativoi");
    }

    Imprimir(distancia, V);

    return;
}
void Imprimir(int distancia[], int n)
{
    printf("Vertice  | Distancia da origem\n");
    for (int i = 0; i < n; ++i)
        printf("%d \t\t %d\n", i, distancia[i]);
}

int ConversaoASCII(int v) // nao deveria existir isso
{
    switch (v) {
    case 48:
        return 0;
        break;
    case 49:
        return 1;
        break;
    case 50:
        return 2;
        break;
    case 51:
        return 3;
        break;
    case 52:
        return 4;
        break;
    case 53:
        return 5;
        break;
    case 54:
        return 6;
        break;
    case 55:
        return 7;
    case 56:
        return 8;
        break;
    case 57:
        return 9;
        break;
    default:
        break;
    }
    return 0;
}
