TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    bellman_ford.cpp

HEADERS += \
    bellman_ford.h
