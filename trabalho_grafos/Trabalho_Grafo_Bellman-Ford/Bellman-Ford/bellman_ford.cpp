#include "bellman_ford.h"


struct Aresta
{
    int origem, destino, peso;
};

struct Grafo
{
    int V, E;  // V-> numero de verticess, E-> Numero de arestas
    struct Aresta* aresta; // representacao do grafo em uma estrutura
};


Bellman_Ford::Bellman_Ford()
{

}

//struct Grafo* Bellman_Ford::CriandoGrafo(int V, int E)
//{
//    struct Grafo* grafo =(struct Grapo*) malloc( sizeof(struct Grafo) );
//    grafo->V = V;
//    grafo->E = E;

//    grafo->aresta = (struct Aresta*) malloc( grafo->E * sizeof( struct Aresta ) );

//    return grafo;
//}

void Bellman_Ford::Executando(struct Grafo *grafo, int origem)
{
    int V = grafo->V;
    int E = grafo->E;
    int distancia[V];

    // Step 1: inicializa a distancia de origem para todos os vertices como infinito
    for (int i = 0; i < V; i++)
        distancia[i] = INT_MAX;
    distancia[origem] = 0;

    // relaxamento de todas as arestas
    for (int i = 1; i <= V - 1; i++)
    {
        for (int j = 0; j < E; j++)
        {
            int u = grafo->aresta[j].origem;
            int v = grafo->aresta[j].destinoino;
            int peso = grafo->aresta[j].peso;
            if (distancia[u] != INT_MAX && distancia[u] + peso < distancia[v])
                distancia[v] = distancia[u] + peso;
        }
    }

    for (int i = 0; i < E; i++)
    {
        int u = grafo->aresta[i].origem;
        int v = grafo->aresta[i].destinoino;
        int peso = grafo->aresta[i].peso;
        if (distancia[u] != INT_MAX && distancia[u] + peso < distancia[v])
            printf("Grafo contem  um peso negativo");
    }

    Imprimir(distancia, V);

    return;
}
void Bellman_Ford::Imprimir(int distancia[], int n)
{
    printf("Distancia da origem do vertice \n");
    for (int i = 0; i < n; ++i)
        printf("%d \t\t %d\n", i, distancia[i]);
}

int Bellman_Ford::ConversaoASCII(int v) // nao deveria existir isso
{
    switch (v) {
    case 48:
        return 0;
        break;
    case 49:
        return 1;
        break;
    case 50:
        return 2;
        break;
    case 51:
        return 3;
        break;
    case 52:
        return 4;
        break;
    case 53:
        return 5;
        break;
    case 54:
        return 6;
        break;
    case 55:
        return 7;
    case 56:
        return 8;
        break;
    case 57:
        return 9;
        break;
    default:
        break;
    }
    return 0;
}
