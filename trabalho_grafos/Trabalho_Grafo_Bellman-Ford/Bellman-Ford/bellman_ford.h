#ifndef BELLMAN_FORD_H
#define BELLMAN_FORD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

class Bellman_Ford
{
public:
    Bellman_Ford();

    // a structure to represent a pesoed aresta in grafo
    struct Aresta
    {
        int origem, destinoino, peso;
    };

    // a structure to represent a connected, directed and
    // pesoed grafo
    struct Grafo
    {
        int V, E;  // V-> numero de verticess, E-> Numero de arestas
        struct Aresta* aresta; // representacao do grafo em uma estrutura
    };

    struct Grafo* CriandoGrafo(int V, int E);
    int ConversaoASCII(int v);
private:

    void Executando(Grafo *grafo, int origem);
    void Imprimir(int distancia[],int n);


};

#endif // BELLMAN_FORD_H
