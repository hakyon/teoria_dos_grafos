#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include <iostream>

#define INFINITO 10000

using namespace std;

class Dijkstra
{
public:
    Dijkstra();

    void Leitura();
    void Inicializar();
    int getClosestUnmarkedNode();
    void calculadistancia();
    void Impressao();
    void printPath(int);

private:
    int adjmatriz[15][15];
    int predecessor[15],distancia[15];
    bool mark[15]; //mantem informacao do no visitado
    int origem;
    int numeroDeVertices;
};

#endif // DIJKSTRA_H
