#include "dijkstra.h"

Dijkstra::Dijkstra()
{

}

void Dijkstra::Leitura()
{
    cout<<"Entre com o numero de vertices do grafo :";
    cin>>numeroDeVertices;
    while(numeroDeVertices <= 0) {
        cout<<"Entre com o numero de vertices do grafo \n";
        cin>>numeroDeVertices;
    }
    cout<<"Entre com os valores da matriz adjacente do grafo\n";
    cout<<"Valor do infinito "<<INFINITO<<endl;
    for(int i=0;i<numeroDeVertices;i++) {
        cout<<"Entre com o peso do vertice para seguinte linha "<<i<<endl;
        for(int j=0;j<numeroDeVertices;j++) {
            cin>>adjmatriz[i][j];
            while(adjmatriz[i][j]<0) {
                cout<<"pesos precisam sermaiores. Entre com o peso novamente\n";
                cin>>adjmatriz[i][j];
            }
        }
    }
    cout<<"Entre com o vertice de origem\n";
    cin>>origem;
    while((origem<0) && (origem>numeroDeVertices-1)) {
        cout<<"Vertice de origem deve ser entre 0 e "<<numeroDeVertices-1<<endl;
        cout<<"Entre com o vertice de origem novamente\n";
        cin>>origem;
    }
}

void Dijkstra::Inicializar()
{
    for(int i=0;i<numeroDeVertices;i++) {
        mark[i] = false;
        predecessor[i] = -1;
        distancia[i] = INFINITO;
    }
    distancia[origem]= 0;
}

int Dijkstra::getClosestUnmarkedNode()
{
    int mindistancia = INFINITO;
    int closestUnmarkedNode;
    for(int i=0;i<numeroDeVertices;i++) {
        if((!mark[i]) && ( mindistancia >= distancia[i])) {
            mindistancia = distancia[i];
            closestUnmarkedNode = i;
        }
    }
    return closestUnmarkedNode;
}

void Dijkstra::calculadistancia()
{
    Inicializar();
    int distanciaMinima = INFINITO;
    int closestUnmarkedNode;
    int count = 0;
    while(count < numeroDeVertices) {
        closestUnmarkedNode = getClosestUnmarkedNode();
        mark[closestUnmarkedNode] = true;
        for(int i=0;i<numeroDeVertices;i++) {
            if((!mark[i]) && (adjmatriz[closestUnmarkedNode][i]>0) ) {
                if(distancia[i] > distancia[closestUnmarkedNode]+adjmatriz[closestUnmarkedNode][i]) {
                    distancia[i] = distancia[closestUnmarkedNode]+adjmatriz[closestUnmarkedNode][i];
                    predecessor[i] = closestUnmarkedNode;
                }
            }
        }
        count++;
    }
}

void Dijkstra::printPath(int node)
{
    if(node == origem)
        cout<<(char)(node + 97)<<"..";
    else if(predecessor[node] == -1)
        cout<<"Nenhum caminho para “<<Origem<<”ate "<<(char)(node + 97)<<endl;
    else {
        printPath(predecessor[node]);
        cout<<(char) (node + 97)<<"..";
    }
}

void Dijkstra::Impressao()
{
    for(int i=0;i<numeroDeVertices;i++) {
        if(i == origem)
            cout<<(char)(origem + 97)<<".."<<origem;
        else
            printPath(i);
        cout<<"->"<<distancia[i]<<endl;
    }
}
