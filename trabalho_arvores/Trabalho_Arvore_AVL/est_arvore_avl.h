#ifndef EST_ARVORE_AVL_H
#define EST_ARVORE_AVL_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
//#include <windows.h>

#include <iostream>
#include <fstream>


using namespace std;

struct No
{
        int info;
        int FB;
        struct No *esq;
        struct No *dir;
};
extern No* noh_raiz;

class Est_Arvore_AVL
{
public:
    Est_Arvore_AVL();

    No* InsereAVL(No* noh, int ch);
    No* Consulta(No* noh, char ch);
    bool Existe(No* noh, int ch);
    No* Remove(No* noh, char ch);
    No* RemoveAVL(No* noh);


    int Altura(No* noh);
    int CalculaFatorBalanceamento(No* noh);
    void setFatorBalanceamento(No* noh);
    No* AjustarArvoreAVL(No* noh);

    No* rotacao_direita(No* noh);
    No* rotacao_esquerda(No* noh);
    No* rotacao_dupla_direita (No* noh);
    No* rotacao_dupla_esquerda (No* noh);
    void impressao(No *noh); // nao querendo usar isso
    void impressaoPosOrdem(No *noh); // nao querendo usar isso
};









#endif // EST_ARVORE_AVL_H
