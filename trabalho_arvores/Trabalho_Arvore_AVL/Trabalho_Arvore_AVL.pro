TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

QT += widgets
INCLUDEPATH += C:\Qt\Qt5.8.0\5.8\mingw53_32\include\QtWidgets


SOURCES += main.cpp \
    est_arvore_avl.cpp \
    mainwindow.cpp

HEADERS += \
    est_arvore_avl.h \
    mainwindow.h

FORMS += \
    mainwindow.ui
