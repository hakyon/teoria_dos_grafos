#include "est_arvore_avl.h"

Est_Arvore_AVL::Est_Arvore_AVL()
{
    // nada por aqui
}




int Est_Arvore_AVL::Altura (No* noh) // verifica a altura entre os lados direito e esquerdo
{
    int Alt_Esq, Alt_Dir;
    if (noh == NULL) return 0;
    else
    {
        Alt_Esq = Altura (noh->esq);
        Alt_Dir = Altura (noh->dir);
        if (Alt_Esq > Alt_Dir)
        {
            return (1 + Alt_Esq);
        }
        else
        {
            return (1 + Alt_Dir);
        }
    }
}

int Est_Arvore_AVL::CalculaFatorBalanceamento(No* noh) // faz o calculo do fator
{
    if(noh == NULL)return 0;
    return (Altura(noh->esq)- Altura(noh->dir));
}

void Est_Arvore_AVL::setFatorBalanceamento(No* noh) // fixa fator de balanceamento no no
{
    if (noh!= NULL)
    {
        noh->FB=(Altura(noh->esq)- Altura(noh->dir));
        setFatorBalanceamento(noh->esq);
        setFatorBalanceamento(noh->dir);
    }
}


No* Est_Arvore_AVL::rotacao_direita(No* N3)
{
    No* N2= N3->esq;
    if(N2->dir) N3->esq = N2->dir;
    else N3->esq=NULL;
    N2->dir=N3;
    return N2;
}

No* Est_Arvore_AVL::rotacao_esquerda(No* N1)
{
    No* N2= N1->dir;
    if(N2->esq) N1->dir= N2->esq;
    else N1->dir=NULL;
    N2->esq=N1;
    return N2;
}

No* Est_Arvore_AVL::rotacao_dupla_direita (No* N3)
{
    No* N1= N3->esq;
    No* N2= N1->dir;

    if(N2->esq) N1->dir= N2->esq;
    else N1->dir=NULL;

    if(N2->dir) N3->esq = N2->dir;
    else N3->esq=NULL;

    N2->esq=N1;
    N2->dir=N3;

    return N2;
}

No* Est_Arvore_AVL::rotacao_dupla_esquerda (No* N1)
{
    No* N3= N1->dir;
    No* N2= N3->esq;

    if(N2->esq) N1->dir= N2->esq;
    else N1->dir=NULL;

    if(N2->dir) N3->esq = N2->dir;
    else N3->esq=NULL;

    N2->esq=N1;
    N2->dir=N3;

    return N2;
}

No* Est_Arvore_AVL::AjustarArvoreAVL(No* noh)
{
    if(noh != NULL)
    {
        noh->FB=CalculaFatorBalanceamento(noh);
        if(noh->FB == 2)
        {
            noh->esq->FB=CalculaFatorBalanceamento(noh->esq);
            if(noh->esq->FB > 0)
            {
                noh = rotacao_direita(noh);
            }
            else
            {
                noh =  rotacao_dupla_direita(noh);
            }
        }
        else if(noh->FB == -2)
        {
            noh->dir->FB=CalculaFatorBalanceamento(noh->dir);
            if(noh->dir->FB < 0)
            {
                noh = rotacao_esquerda(noh);
            }
            else
            {
                noh =  rotacao_dupla_esquerda(noh);
            }
        }
        noh->esq = AjustarArvoreAVL(noh->esq);
        noh->dir = AjustarArvoreAVL(noh->dir);
    }
    return noh;
}


No* Est_Arvore_AVL::InsereAVL(No* noh, int ch)
{
    if (noh == NULL)
    {
        noh =(No*)malloc(sizeof(No));
        noh->info= ch;
        noh->FB= 0;
        noh->esq= NULL;
        noh->dir= NULL;
        return noh;
    }
    else
    {
        if(ch < noh->info)
        {
            noh->esq = InsereAVL(noh->esq,ch);
        }
        else
        {
            noh->dir = InsereAVL(noh->dir,ch);
        }
    }
    return noh;
}


No* Est_Arvore_AVL::Consulta(No* noh, char ch)
{
    while (noh != NULL)
    {
        if(ch == noh->info)
        {
            return noh;
        }
        else
        {
            if(ch < noh->info)
            {
                noh=noh->esq;
            }
            else
            {
                noh=noh->dir;
            }
        }
    }
    return NULL;
}


No* Est_Arvore_AVL::Remove(No* noh, char ch)
{
    No* nohAux=noh;
    No* nohPai;
    bool bdir=false;

    if(ch == noh->info) return RemoveAVL(nohAux);

    while (nohAux != NULL)
    {
        if(ch == nohAux->info)
        {
            if(bdir) nohPai->dir= RemoveAVL(nohAux);
            else nohPai->esq= RemoveAVL(nohAux);
            return noh;
        }
        else
        {
            if(ch < nohAux->info)
            {
                bdir=false;
                nohPai=nohAux;
                nohAux=nohAux->esq;
            }
            else
            {
                bdir=true;
                nohPai=nohAux;
                nohAux=nohAux->dir;
            }
        }
    }
    return noh;
}


No* Est_Arvore_AVL::RemoveAVL(No* noh) // desmontar a arvore
{
    No* nohAux;
    No* nohAuxPai;

    if((noh->esq == NULL) && (noh->dir == NULL))
    {
        free(noh);
        return NULL;
    }
    else if((noh->esq == NULL) && (noh->dir != NULL))
    {
        nohAux=noh->dir;
        free(noh);
        return nohAux;
    }
    else if((noh->esq != NULL) && (noh->dir == NULL))
    {
        nohAux=noh->esq;
        free(noh);
        return nohAux;
    }
    else
    {
        if(noh->esq->dir == NULL)
        {
            nohAux=noh->esq;
            noh->esq->dir=noh->dir;
            free(noh);
            return nohAux;
        }
        else
        {
            nohAux=noh->esq;

            while (nohAux->dir != NULL)
            {
                nohAuxPai=nohAux;
                nohAux=nohAux->dir;
            }

            if(nohAux->esq!=NULL) nohAuxPai->dir=nohAux->esq;
            else nohAuxPai->dir=NULL;

            nohAux->dir=noh->dir;
            nohAux->esq=noh->esq;

            free(noh);
            return nohAux;
        }
    }

    return NULL;
}

void Est_Arvore_AVL::impressao(No *noh)
{
    if(noh)
    {
        impressao(noh->esq);
        cout << noh->info << endl;
        impressao(noh->dir);
    }
    else
        return;
}

void Est_Arvore_AVL::impressaoPosOrdem(No *noh)
{
    if(noh)
    {
        impressao(noh->esq);
        impressao(noh->dir);
        cout << noh->info << endl;
    }
    else
        return;
}


bool Est_Arvore_AVL::Existe(No* noh, int ch)
{
    while (noh != NULL)
    {
        if(ch == noh->info)
        {
            return true;
        }
        else
        {
            if(ch < noh->info)
            {
                noh=noh->esq;
                Existe(noh,ch);
            }
            else
            {
                noh=noh->dir;
                Existe(noh,ch);
            }
        }
    }
    return false;
}
