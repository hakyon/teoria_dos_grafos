#include "est_arvore_b.h"

est_arvore_b::est_arvore_b()
{

}

void  est_arvore_b::insere_chave(NoArvoreB *raiz, int info, NoArvoreB *filhodir) {
    int k, pos;

    //busca para obter a posição ideal para inserir a nova chave
    //pos = busca_binaria(raiz, info);
    k = raiz->num_chaves;

    //realiza o remanejamento para manter as chaves ordenadas
    while (k > pos && info < raiz->chaves[k-1])
    {
        raiz->chaves[k] = raiz->chaves[k-1];
        raiz->filhos[k+1] = raiz->filhos[k];
        k--;
    }
    //insere a chave na posição ideal
    raiz->chaves[pos] = info;
    raiz->filhos[pos+1] = filhodir;
    raiz->num_chaves++;
}


//Realiza a busca do nó para inserir a chave e faz as subdivisões quando necessárias
NoArvoreB* est_arvore_b::insere(NoArvoreB *raiz, int info, int *h, int *info_retorno) {
    int i, pos, info_mediano; //auxiliar para armazenar a chave que irá subir para o pai
    NoArvoreB *temp, *filho_dir; //ponteiro para o filho à direita da chave

    if (raiz == NULL) {
        //O nó anterior é o ideal para inserir a nova chave (chegou em um nó folha)
        *h = 1;
        *info_retorno = info;
        return NULL;
    }
    else {
        //pos = busca_binaria(raiz,info);
        if (raiz->num_chaves > pos && raiz->chaves[pos] == info) {
            printf("Chave já contida na Árvore");
            *h = 0;
        }
        else {
            //desce na árvore até encontrar o nó folha para inserir a chave.
            filho_dir = insere(raiz->filhos[pos],info,h,info_retorno);
            //Se true deve inserir a info_retorno no nó.
            if (*h) {
                //Tem espaço na página
                if (raiz->num_chaves < MAX_CHAVES) {
                    insere_chave(raiz, *info_retorno, filho_dir);
                    *h = 0;
                }
                else { //Overflow. Precisa subdividir
                    temp = (NoArvoreB*) malloc (sizeof(NoArvoreB));
                    temp->num_chaves = 0;

                    //inicializa filhos com NULL
                    for (i = 0; i < MAX_FILHOS; i++)
                        temp->filhos[i] = NULL;

                    //elemento mediano que vai subir para o pai
                    info_mediano = raiz->chaves[MIN_CHAVES];

                    //insere metade do nó raiz no temp (efetua subdivisão)
                    temp->filhos[0] = raiz->filhos[MIN_CHAVES+1];

                    for (i = MIN_CHAVES + 1; i < MAX_CHAVES; i++)
                        insere_chave(temp, raiz->chaves[i], raiz->filhos[i+1]);

                    //atualiza nó raiz.
                    for (i = MIN_CHAVES; i<MAX_CHAVES; i++) {
                        raiz->chaves[i] = 0;
                        raiz->filhos[i+1] = NULL;
                    }
                    raiz->num_chaves = MIN_CHAVES;

                    //Verifica em qual nó será inserida a nova chave
                    if (pos <= MIN_CHAVES)
                        insere_chave(raiz, *info_retorno, filho_dir);
                    else
                        insere_chave(temp, *info_retorno, filho_dir);

                    //retorna o mediano para inserí-lo no nó pai e o temp como filho direito do mediano.
                    *info_retorno = info_mediano;
                    return temp;
                }
            }
        }
    }
    return NULL;
}

NoArvoreB* est_arvore_b::insere_arvoreB(NoArvoreB *raiz, int info) {
    int h;
    int info_retorno, i;
    NoArvoreB *filho_dir, *nova_raiz;

    filho_dir = insere(raiz,info,&h,&info_retorno);
    //Aumetará a altura da árvore
    if (h) {
        nova_raiz = (NoArvoreB*) malloc (sizeof(NoArvoreB));
        nova_raiz->num_chaves = 1;
        nova_raiz->chaves[0] = info_retorno;
        nova_raiz->filhos[0] = raiz;
        nova_raiz->filhos[1] = filho_dir;
        for (i = 2; i <= MAX_CHAVES; i++)
            nova_raiz->filhos[i] = NULL;
        return nova_raiz;
    }
    else
        return raiz;
}

// MUDAR O VALOR DE T para 2 no DEFINE!!!
// Insere 1,2,3,5,4,10
