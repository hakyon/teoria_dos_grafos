#ifndef EST_ARVORE_B_H
#define EST_ARVORE_B_H

#include <stdio.h>
#include <stdlib.h>

#define T 2
#define MAX_CHAVES  2 * T - 1 //Quantidade máxima de chaves
#define MAX_FILHOS 2 * T //Quantidade máxima de filhos
#define MIN_CHAVES T - 1 //Ocupação mínima em cada nó


typedef struct no_arvore_b_ {
    int num_chaves; //Quantidades de chaves contida no nó
    int chaves[MAX_CHAVES]; //Chaves armazenadas no nó
    struct no_arvore_b_* filhos[MAX_FILHOS]; //Ponteiro para os filhos
} NoArvoreB;

class est_arvore_b
{
public:
    est_arvore_b();

    void insere_chave(NoArvoreB *raiz, int info, NoArvoreB *filhodir);
    NoArvoreB *insere(NoArvoreB *raiz, int info, int *h, int *info_retorno);
    NoArvoreB *insere_arvoreB(NoArvoreB *raiz, int info);
    void Impressao(NoArvoreB *raiz);
    NoArvoreB busca(NoArvoreB *raiz, int info);
    NoArvoreB *Remover(NoArvoreB *raiz, int info);
};

#endif // EST_ARVORE_B_H
